#!/usr/bin/python3
import os
import sys
import json
import yaml
import argparse

# Parse the command line arguments we've been given
parser = argparse.ArgumentParser(description='Utility to determine which jobs need to be registered in Jenkins.')
parser.add_argument('--products', type=str, required=True)
arguments = parser.parse_args()

# Our output will be a list of Dictionaries, containing several keys:
# 1) The name of the job
# 2) The name of the product we are building
# 3) The list of rules to use when cloning repositories (for use with git-kclone)
jobsGathered = []

# Go over all of our products and process them
knownProducts = os.listdir( arguments.products )
for product in knownProducts:
	# Load the details for this Product...
	productFile = open( os.path.join( arguments.products, product ), 'r' )
	productConfig = yaml.load( productFile )

	# Trim the YAML off the filename so we can use it further below...
	product = product.rstrip('.yaml')

	# Build the list of rules we'll need for cloning repositories...
	repositoriesToClone = ' '.join( productConfig['contains'] )

	# Create a job for each of our named version targets
	# Usually this is just used for devel and stable...
	for versionName, versionTarget in productConfig['versions'].items():
		# Build a description for our job...
		jobDescription = "API Generation Job for {0} - {1} branch".format( product, versionTarget )
		# Create the definition for our job...
		jobDefinition = {
			'project': product,
			'version': versionName,
			'branch': versionTarget,
			'description': jobDescription,
			'generator': productConfig['generator'],
			'repositoriesToClone': repositoriesToClone
		}
		# And add it to the list of found jobs...
		jobsGathered.append( jobDefinition )

	# Now that we have that added, we can also add a generic, parameterised, job that allows building an arbitrary branch
	# To signal this to the DSL we simply omit the version/branch keys
	jobDescription = "API Generation Job for {0} - Bespoke branch".format( product )
	jobDefinition = {
		'project': product,
		'description': jobDescription,
		'generator': productConfig['generator'],
		'repositoriesToClone': repositoriesToClone
	}
	jobsGathered.append( jobDefinition )

# Now output the jobs we've gathered in JSON to disk
# This will subsequently be read in by a Jenkins DSL script and turned into Jenkins Jobs
filePath = os.path.join( os.getcwd(), 'gathered-jobs.json')
with open(filePath, 'w') as jobsFile:
	json.dump( jobsGathered, jobsFile, sort_keys=True, indent=2  )

# All done!
sys.exit(0)
	
